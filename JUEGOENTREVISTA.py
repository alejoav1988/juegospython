import pygame, sys
from pygame.locals import *

class DIBUJO(object):
		def draw(DIBUJAR,VENTANA):
			VENTANA.blit(DIBUJAR(DIBUJAR.x,DIBUJAR.y))

pygame.init()

FPS = 30
frecuencia = pygame.time.Clock()

PANTALLA = pygame.display.set_mode((1200, 800),0,32)
pygame.display.set_caption('Juego Entrevista')

CIELO = (0,255,255)
NUBE = (255,255,255)
ROCA = (255,255,255)
SOL = (255,255,0)
ROJO = (255,0,0)

PERSONAJE = pygame.image.load("sprites/personaje.png")
ARBUSTO = pygame.image.load("sprites/arbusto.png")
META = pygame.image.load("sprites/Meta.png")
PERSONAJERECT = PERSONAJE.get_rect()

PERSONAJEX = 100
PERSONAJEY = 300
ARBUSTOX = 100
ARBUSTOY = 450
METAX = 900
METAY = 500

NUBE1_DIM = [100,50,200,100]
NUBE2_DIM = [300,100,200,100]

aumX = 0
aumY = 0

fontObject = pygame.font.Font("sprites/StripeAttack.ttf",40)
textSurfaceObject = fontObject.render("Juego Entrevista",True,ROJO,CIELO)
textRectObject = textSurfaceObject.get_rect()
textRectObject.center = (200,30)

fontObject2 = pygame.font.Font("sprites/StripeAttack.ttf",60)
textSurfaceObject2 = fontObject2.render("Has Ganado el Juego",True,ROJO,CIELO)
textRectObject2 = textSurfaceObject.get_rect()
textRectObject2.center = (400,300)

fontObject3 = pygame.font.Font("sprites/StripeAttack.ttf",60)
textSurfaceObject3 = fontObject3.render("Has chocado con una roca",True,ROJO,CIELO)
textRectObject3 = textSurfaceObject.get_rect()
textRectObject3.center = (400,300)

d = DIBUJO()

while True: 
	PANTALLA.fill(CIELO)
	pygame.draw.circle(PANTALLA,SOL,(1100,50),120,0)
	pygame.draw.ellipse(PANTALLA,NUBE,NUBE1_DIM,0)
	pygame.draw.ellipse(PANTALLA,NUBE,NUBE2_DIM,0)
	
	pygame.draw.circle(PANTALLA,ROCA,(0,500),120,0)

	PANTALLA.blit(ARBUSTO,(ARBUSTOX,ARBUSTOY))
	PANTALLA.blit(PERSONAJE, (PERSONAJEX,PERSONAJEY))
	PANTALLA.blit(META, (METAX,METAY))

	PERSONAJEX = PERSONAJEX + aumX
	PERSONAJEX = PERSONAJEX + aumX
	if PERSONAJEX >= 1100:
		PANTALLA.blit(textSurfaceObject2,textRectObject2)
		PERSONAJERECT.move_ip(-100,-100)
	elif PERSONAJEX < 0:
		PANTALLA.blit(textSurfaceObject3,textRectObject3)
		PERSONAJERECT.move_ip(-100,-100)
	for event in pygame.event.get():
		if event.type == QUIT:
			# pygame.mixer.music.stop()
			pygame.quit()
			sys.exit()
		elif event.type == pygame.KEYDOWN:
			if event.key == pygame.K_RIGHT:
				aumX = 5
				PERSONAJERECT = PERSONAJERECT.move(aumX,0)			
			if event.key == pygame.K_LEFT:
				aumX = -5
				PERSONAJERECT = PERSONAJERECT.move(aumX,0)

	pygame.display.update()
	frecuencia.tick(FPS)

